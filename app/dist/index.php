<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel=icon href=/app/dist/img/favicon.png sizes="216x216" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

        <link rel="stylesheet" href="/app/dist/css/fonts.min.css">
        <link rel="stylesheet" href="/app/dist/css/main.min.css">

        <title>Natours | Exciting tours for adventurous people</title>
    </head>
    <body>

    <header class="header df">
        <div class="logo">
            <? if ($_SERVER['REQUEST_URI'] == '/') {?>
                <span class="logo__wrap df">
                    <img class="logo__img" src="/app/dist/img/logo-white.png" alt="logo">
                </span>
            <? } else { ?>
                <a class="logo__wrap df" href="/">
                    <img class="logo__img" src="/app/dist/img/logo-white.png" alt="logo">
                </a>
            <?}?>
        </div>
        <div class="wrapper">
            <div class="row">
                <div class="column">
                    <div class="headerContent">
                        <div class="headerContent__row df">
                            <h1 class="headerTitle">
                                <span class="headerTitle__one headerTitle__one-animLeft">outdoors</span>
                                <span class="headerTitle__two headerTitle__two-animRight">is where life happens</span>
                            </h1>
                            <button class="btn btn-animBot btn-grey">
                                discover all tours
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <svg class="svgLine__container svgLine__container-header" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1890 150">
            <g transform="translate(0,-902.36218)"></g>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 1925,0 0,150 -1925,0"></path>
        </svg>
    </header>
    <main class="main">

        <section class="about df">
            <div class="wrapper">
                <div class="row about__row">
                    <div class="about__title df">
                        <h2 class="title title-about">exiting tours for adventurous people</h2>
                    </div>
                    <div class="column-2">
                        <div class="aboutText about-margin df">
                            <h3 class="aboutText__title">your going to fall in love with nature</h3>
                            <p class="aboutText__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet beatae cum facere fuga incidunt magnam molestiae obcaecati officia perferendis, quod unde vel? Asperiores, eos non nostrum omnis quas saepe!</p>
                            <h3 class="aboutText__title">live adventures like you never have before</h3>
                            <p class="aboutText__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis deleniti facilis ipsa magnam mollitia nobis possimus recusandae repudiandae, sequi voluptatem.</p>
                            <a href="#/" class="aboutText__link">learn more</a>
                        </div>
                    </div>
                    <div class="column-2">
                        <div class="aboutImage about-margin df">
                            <?for ($i=1; $i <= 3; $i++){?>
                                <figure class="aboutImage__figure aboutImage__figure-<?= $i ?> df">
                                    <img

                                            srcset="/app/dist/img/nat-<?= $i ?>.jpg 300w,
                                                    /app/dist/img/nat-<?= $i ?>-large.jpg 1000w"

                                            sizes="(max-width: 768px) 300px,
                                                  (max-width: 1024px) 1000px,  1000px"

                                            src="/app/dist/img/nat-<?= $i ?>-large.jpg"
                                            class="aboutImage__figure-img aboutImage__figure-shadow"
                                            alt="img-<?= $i ?>"
                                    >
                                </figure>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section class="features df">
            <svg class="svgLine__container svgLine__container-features" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1890 150">
                <g transform="translate(0,-902.36218)"></g>
                <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                <path d="M 0,150 0,0 1925,0"></path>
            </svg>

            <div class="wrapper">
                <div class="row">
                    <div class="column-4">
                        <div class="featuresItem featuresItem-shadow df">
                            <div class="featuresItem__imgBox df">
                                <i class="featuresItem__icon featuresItem__icon-world"></i>
                            </div>
                            <h3 class="featuresItem__title">Explore the world</h3>
                            <p class="featuresItem__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A distinctio enim eum ex iure molestiae.</p>
                        </div>
                    </div>
                    <div class="column-4">
                        <div class="featuresItem featuresItem-shadow df">
                            <div class="featuresItem__imgBox df">
                                <i class="featuresItem__icon featuresItem__icon-compass"></i>
                            </div>
                            <h3 class="featuresItem__title">Meet Nature</h3>
                            <p class="featuresItem__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                        </div>
                    </div>
                    <div class="column-4">
                        <div class="featuresItem featuresItem-shadow df">
                            <div class="featuresItem__imgBox df">
                                <i class="featuresItem__icon featuresItem__icon-maps"></i>
                            </div>
                            <h3 class="featuresItem__title">Find your way</h3>
                            <p class="featuresItem__subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A distinctio enim eum ex iure molestiae, provident quae quo repellendus sit.</p>
                        </div>
                    </div>
                    <div class="column-4">
                        <div class="featuresItem featuresItem-shadow df">
                            <div class="featuresItem__imgBox df">
                                <i class="featuresItem__icon featuresItem__icon-heart"></i>
                            </div>
                            <h3 class="featuresItem__title">Live a Healthier life</h3>
                            <p class="featuresItem__subtitle">Lorem ipsum dolor sit amet. A distinctio enim eum ex iure molestiae, provident quae quo repellendus sit.</p>
                        </div>
                    </div>
                </div>
            </div>

            <svg class="svgLine__container svgLine__container-features" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1890 150">
                <g transform="translate(0,-902.36218)"></g>
                <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                <path d="m 1925,0 0,150 -1925,0"></path>
            </svg>
        </section>

        <section class="subscription">
            <div class="subscription__imgBox">
                <div class="wrapper">
                    <div class="row">
                        <div class="column">
                            <div class="mainFormBox df">
                                <h2 class="title title-subscript">how can we contact with you?</h2>

                                <form action="" method="post" class="formBox df">

                                    <fieldset class="form">
                                        <div class="form__horizontal df">
                                            <label class="form__label"
                                                   for="name">
                                                What is your name:
                                            </label>
                                            <div class="form__holder">
                                                <input class="form__control"
                                                       id="name"
                                                       type="text"
                                                       name="name"
                                                       value=""
                                                       required="required"
                                                />
                                            </div>
                                            <div class="form__message" id="nameMessage"></div>
                                        </div>

                                        <div class="form__horizontal df">
                                            <label class="form__label" for="find">
                                                How did you find us?
                                            </label>
                                            <div class="form__holder">
                                                <select class="form__control"
                                                        id="find"
                                                        name="find"
                                                        data-error-message="Поле заполнено некорректно"
                                                        required="required">
                                                    <option value="From friends">From friends</option>
                                                    <option value="From social networks">From social networks</option>
                                                    <option value="From advertisement">From advertisement</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form__horizontal df form-group">
                                            <label class="form__label" for="receive">
                                                How do you want to receive news?
                                            </label>
                                            <div class="form__holder">
                                                <select class="form__control"
                                                        id="receive"
                                                        name="receive"
                                                        required="required">

                                                    <option value="email">By mail</option>
                                                    <option value="sms">By SMS</option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form__horizontal df">
                                            <label class="form__label">
                                                 <span id="contactLabelText">Share your email with us</span>
                                            </label>
                                            <div class="form__holder">
                                                <input class="form__control"
                                                       id="userContact"
                                                       type="text"
                                                       name="userContact"
                                                       value=""
                                                       required="required"/>
                                            </div>
                                            <div class="form__message" id="userContactMessage"></div>
                                        </div>
                                    </fieldset>

                                    <div class="submitBox">
                                        <input type="submit"
                                               id="submit"
                                               class="btn btn-light-green"
                                               value="I want to receive news">
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="/app/dist/js/bundleMain.js"></script>
    </main>
    </body>
</html>