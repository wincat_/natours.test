import { BaseValidator } from "./BaseValidator";

export class EmailValidator extends BaseValidator {
    /**
     * Validation email field
     *
     * @param value
     */
    validation(value: string):any {
        let errors: any = {};
        if(!this.isRequired(value)) {
            errors.required = true;
        }
        if(!this.isTypeMail(value)) {
            errors.email = true;
        }
        if(!this.isEmpty(errors)) {

            return this.responseError(errors);
        }

        return true;
    }
}