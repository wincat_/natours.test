import { BaseValidator } from "./BaseValidator";

export class NameValidator extends BaseValidator {
    /**
     * Validation name field
     *
     * @param value
     */
    validation(value: string):any {
        let errors: any = {};
        if(!this.isRequired(value)) {
            errors.required = true;
        }
        if(!this.isTypeString(value)) {
            errors.string = true;
        }
        if(!this.isEmpty(errors)) {

            return this.responseError(errors);
        }

        return true;
    }
}