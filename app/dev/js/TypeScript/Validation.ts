export interface StringValidator {
    /**
     * Init isRequired
     *
     * @param s
     */
    isRequired(s: string): boolean;

    /**
     * Init isTypeString
     *
     * @param s
     */
    isTypeString(s: string): boolean;

    /**
     * Init isTypeMail
     * @param s
     */
    isTypeMail(s: string): boolean;

    /**
     * Init isTypePhone
     * @param s
     */
    isTypePhone(s: string): boolean;

    /**
     * Init responseError
     * @param e
     */
    responseError(e: object): object;

    /**
     * Init isEmpty
     * @param e
     */
    isEmpty(e: object): boolean
}