import { StringValidator } from "./Validation";

export const requiredRegexp = /^(?!\s*$).+$/;
export const stringRegexp = /^[a-zA-Zа-яА-Я ]+$/;
export const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const phoneRegexp = /\+\(\d{3}\)\d{9}$/;

export class BaseValidator implements StringValidator {
    /**
     * Сheck field required
     *
     * @param s
     */
    isRequired(s: string) {

        return requiredRegexp.test(s);
    }

    /**
     * Сheck field type string
     *
     * @param s
     */
    isTypeString(s: string) {

        return stringRegexp.test(s);
    }

    /**
     * Сheck field type mail
     *
     * @param s
     */
    isTypeMail(s: string) {

        return emailRegexp.test(s);
    }

    /**
     * Сheck field type phone
     *
     * @param s
     */
    isTypePhone(s: string) {

        return phoneRegexp.test(s);
    }

    /**
     * Сheck obj is empty
     *
     * @param obj
     */
    isEmpty(obj: object) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }

        return true;
    }

    /**
     * Check response error
     *
     * @param e
     */
    responseError(e: object) {

        return {
            'errors': e
        };
    }
}