import { BaseValidator } from "./BaseValidator";

export class PhoneValidator extends BaseValidator {
    /**
     * Validation phone field
     *
     * @param value
     */
    validation(value: string):any {
        let errors: any = {};
        if(!this.isRequired(value)) {
            errors.required = true;
        }

        if(!this.isTypePhone(value)) {
            errors.phone = true;
        }

        if(!this.isEmpty(errors)) {

            return this.responseError(errors);
        }

        return true;
    }
}