import { NameValidator } from "./NameValidator";
import { EmailValidator } from "./EmailValidator";
import { PhoneValidator } from "./PhoneValidator";

const validationByName = new NameValidator();
const validationByEmail = new EmailValidator();
const validationByPhone = new PhoneValidator();

const formIsError: any = {
    'name': true,
    'email': true
};
const sendDataUrl = '/';

/**
 * Form input name validation
 *
 * @param value
 */
function nameValidation(value)
{
    let nameEll = (<HTMLInputElement>document.getElementById('name')),
        nameMessageId = (<HTMLInputElement>document.getElementById('nameMessage'));

    nameEll.classList.remove('form__control-error');
    nameEll.classList.remove('form__control-success');

    if(validationByName.validation(value).errors == undefined) {
        nameEll.classList.add('form__control-success');
        nameMessageId.innerHTML = "";
        delete formIsError.name;
    } else {
        nameEll.classList.add('form__control-error');
        nameEll.classList.remove('form__control-success');

        let message = '';
        if(validationByName.validation(value).errors.string != undefined) {
            message = 'Name is not string';
        }
        if(validationByName.validation(value).errors.required != undefined) {
            message = 'Name is required';
        }
        formIsError.name = true;

        nameMessageId.innerHTML = `<div class="message message-error message-show">${message}</div>`;
    }
}

/**
 * Form input email validation
 *
 * @param value
 */
function emailValidation(value)
{
    let userContactEll = (<HTMLInputElement>document.getElementById('userContact')),
        userContactMessageId = (<HTMLInputElement>document.getElementById('userContactMessage'));

    userContactEll.classList.remove('form__control-error');
    userContactEll.classList.remove('form__control-success');

    if(validationByEmail.validation(value).errors == undefined) {
        userContactEll.classList.remove('form__control-error');
        userContactEll.classList.add('form__control-success');
        userContactMessageId.innerHTML = "";
        delete formIsError.email;
    } else {
        let message = '';
        if(validationByEmail.validation(value).errors.email != undefined) {
            message = 'Email is not valid';
            userContactEll.classList.add('form__control-error');
        }

        if(validationByEmail.validation(value).errors.required != undefined) {
            message = 'Email is required';
            userContactEll.classList.add('form__control-error');
        }
        formIsError.email = true;
        delete formIsError.phone;

        userContactMessageId.innerHTML = `<div class="message message-error message-show">${message}</div>`;
    }
}

/**
 * Form input phone validation
 *
 * @param value
 */
function phoneValidation(value)
{
    let userContactEll = (<HTMLInputElement>document.getElementById('userContact')),
        userContactMessageId = (<HTMLInputElement>document.getElementById('userContactMessage'));

    userContactEll.classList.remove('form__control-error');
    userContactEll.classList.remove('form__control-success');

    if(validationByPhone.validation(value).errors == undefined) {
        userContactEll.classList.remove('form__control-error');
        userContactEll.classList.add('form__control-success');
        userContactMessageId.innerHTML = "";
        delete formIsError.phone;
    } else {
        let message = '';
        if(validationByPhone.validation(value).errors.phone != undefined) {
            message = 'Phone is not valid';
            userContactEll.classList.add('form__control-error');
        }
        if(validationByPhone.validation(value).errors.required != undefined) {
            message = 'Phone is required';
            userContactEll.classList.add('form__control-error');
        }
        formIsError.phone = true;
        delete formIsError.email;

        userContactMessageId.innerHTML = `<div class="message message-error message-show">${message}</div>`;
    }
}

/**
 * Сheck obj is empty
 *
 * @param obj
 */
function isEmpty(obj: object)
{
    for(let key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }

    return true;
}

/**
 * Form submit
 */
function submit(event, data)
{
    if (isEmpty(formIsError)) {
        let XHR = new XMLHttpRequest();
        let formData  = new FormData();
        formData.append('name', data.name);
        formData.append('find', data.find);
        formData.append('receive', data.receive);
        formData.append('userContact', data.userContact);

        // Define what happens on successful data submission
        XHR.addEventListener('load', function() {
            // Yeah! Data sent and response loaded.
        });

        // Define what happens in case of error
        XHR.addEventListener('error', function() {
            // Oops! Something went wrong.
        });

        XHR.open("POST", sendDataUrl);
        XHR.send(formData);
    }
}

/**
 * Event by name field
 *
 * @param event
 */
function inputNameEvent(event)
{
    let inputValue = event.target.value;
    nameValidation(inputValue);
}

/**
 * Event by user contact field
 *
 * @param event
 */
function inputUserContactEvent(event)
{
    let inputValue = event.target.value,
        changeReceive = (<HTMLInputElement>document.getElementById('receive')).value;

    if(changeReceive == 'email') {
        emailValidation(inputValue);
    } else {
        phoneValidation(inputValue);
    }
}

// Init event form
let changeInputName = (<HTMLInputElement>document.getElementById('name'));
let changeReceive = (<HTMLInputElement>document.getElementById('receive'));
let changeUserContact = (<HTMLInputElement>document.getElementById('userContact'));

// Init event click by name field
changeInputName.addEventListener('click', (event) => {
    inputNameEvent(event);
});

// Init event change by name field
changeInputName.addEventListener('change', (event) => {
    inputNameEvent(event);
});

// Init event change by receive field
changeReceive.addEventListener('change', (event) => {
    let contactLabelText = (<HTMLInputElement>document.getElementById('contactLabelText')),
        label = 'email';

    changeUserContact.value = '';
    if(changeReceive.value == 'sms') {
        label = `phone number`;
    }
    // contactLabelText.innerHTML = label;
    contactLabelText.innerHTML = `Share your ${label} with us`;

    inputUserContactEvent(event);
});

// Init event click by userContact field
changeUserContact.addEventListener('click', (event) => {
    inputUserContactEvent(event);
});

// Init event change by userContact field
changeUserContact.addEventListener('change', (event) => {
    inputUserContactEvent(event);
});

let formSubmit = document.getElementById('submit');
// Init event change by userContact field
formSubmit.addEventListener('click', (event) => {
    event.preventDefault();
    let data = {
        'name': (<HTMLInputElement>document.getElementById('name')).value,
        'find': (<HTMLInputElement>document.getElementById('find')).value,
        'receive': (<HTMLInputElement>document.getElementById('receive')).value,
        'userContact': (<HTMLInputElement>document.getElementById('userContact')).value
    };
    submit(event, data);
});