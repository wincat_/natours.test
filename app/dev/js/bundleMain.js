(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requiredRegexp = /^(?!\s*$).+$/;
exports.stringRegexp = /^[a-zA-Zа-яА-Я ]+$/;
exports.emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
exports.phoneRegexp = /\+\(\d{3}\)\d{9}$/;
class BaseValidator {
    /**
     * Сheck field required
     *
     * @param s
     */
    isRequired(s) {
        return exports.requiredRegexp.test(s);
    }
    /**
     * Сheck field type string
     *
     * @param s
     */
    isTypeString(s) {
        return exports.stringRegexp.test(s);
    }
    /**
     * Сheck field type mail
     *
     * @param s
     */
    isTypeMail(s) {
        return exports.emailRegexp.test(s);
    }
    /**
     * Сheck field type phone
     *
     * @param s
     */
    isTypePhone(s) {
        return exports.phoneRegexp.test(s);
    }
    /**
     * Сheck obj is empty
     *
     * @param obj
     */
    isEmpty(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    /**
     * Check response error
     *
     * @param e
     */
    responseError(e) {
        return {
            'errors': e
        };
    }
}
exports.BaseValidator = BaseValidator;
},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseValidator_1 = require("./BaseValidator");
class EmailValidator extends BaseValidator_1.BaseValidator {
    /**
     * Validation email field
     *
     * @param value
     */
    validation(value) {
        let errors = {};
        if (!this.isRequired(value)) {
            errors.required = true;
        }
        if (!this.isTypeMail(value)) {
            errors.email = true;
        }
        if (!this.isEmpty(errors)) {
            return this.responseError(errors);
        }
        return true;
    }
}
exports.EmailValidator = EmailValidator;
},{"./BaseValidator":1}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseValidator_1 = require("./BaseValidator");
class NameValidator extends BaseValidator_1.BaseValidator {
    /**
     * Validation name field
     *
     * @param value
     */
    validation(value) {
        let errors = {};
        if (!this.isRequired(value)) {
            errors.required = true;
        }
        if (!this.isTypeString(value)) {
            errors.string = true;
        }
        if (!this.isEmpty(errors)) {
            return this.responseError(errors);
        }
        return true;
    }
}
exports.NameValidator = NameValidator;
},{"./BaseValidator":1}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseValidator_1 = require("./BaseValidator");
class PhoneValidator extends BaseValidator_1.BaseValidator {
    /**
     * Validation phone field
     *
     * @param value
     */
    validation(value) {
        let errors = {};
        if (!this.isRequired(value)) {
            errors.required = true;
        }
        if (!this.isTypePhone(value)) {
            errors.phone = true;
        }
        if (!this.isEmpty(errors)) {
            return this.responseError(errors);
        }
        return true;
    }
}
exports.PhoneValidator = PhoneValidator;
},{"./BaseValidator":1}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const NameValidator_1 = require("./NameValidator");
const EmailValidator_1 = require("./EmailValidator");
const PhoneValidator_1 = require("./PhoneValidator");
const validationByName = new NameValidator_1.NameValidator();
const validationByEmail = new EmailValidator_1.EmailValidator();
const validationByPhone = new PhoneValidator_1.PhoneValidator();
const formIsError = {
    'name': true,
    'email': true
};
const sendDataUrl = '/';
/**
 * Form input name validation
 *
 * @param value
 */
function nameValidation(value) {
    let nameEll = document.getElementById('name'), nameMessageId = document.getElementById('nameMessage');
    nameEll.classList.remove('form__control-error');
    nameEll.classList.remove('form__control-success');
    if (validationByName.validation(value).errors == undefined) {
        nameEll.classList.add('form__control-success');
        nameMessageId.innerHTML = "";
        delete formIsError.name;
    }
    else {
        nameEll.classList.add('form__control-error');
        nameEll.classList.remove('form__control-success');
        let message = '';
        if (validationByName.validation(value).errors.string != undefined) {
            message = 'Name is not string';
        }
        if (validationByName.validation(value).errors.required != undefined) {
            message = 'Name is required';
        }
        formIsError.name = true;
        nameMessageId.innerHTML = `<div class="message message-error message-show">${message}</div>`;
    }
}
/**
 * Form input email validation
 *
 * @param value
 */
function emailValidation(value) {
    let userContactEll = document.getElementById('userContact'), userContactMessageId = document.getElementById('userContactMessage');
    userContactEll.classList.remove('form__control-error');
    userContactEll.classList.remove('form__control-success');
    if (validationByEmail.validation(value).errors == undefined) {
        userContactEll.classList.remove('form__control-error');
        userContactEll.classList.add('form__control-success');
        userContactMessageId.innerHTML = "";
        delete formIsError.email;
    }
    else {
        let message = '';
        if (validationByEmail.validation(value).errors.email != undefined) {
            message = 'Email is not valid';
            userContactEll.classList.add('form__control-error');
        }
        if (validationByEmail.validation(value).errors.required != undefined) {
            message = 'Email is required';
            userContactEll.classList.add('form__control-error');
        }
        formIsError.email = true;
        delete formIsError.phone;
        userContactMessageId.innerHTML = `<div class="message message-error message-show">${message}</div>`;
    }
}
/**
 * Form input phone validation
 *
 * @param value
 */
function phoneValidation(value) {
    let userContactEll = document.getElementById('userContact'), userContactMessageId = document.getElementById('userContactMessage');
    userContactEll.classList.remove('form__control-error');
    userContactEll.classList.remove('form__control-success');
    if (validationByPhone.validation(value).errors == undefined) {
        userContactEll.classList.remove('form__control-error');
        userContactEll.classList.add('form__control-success');
        userContactMessageId.innerHTML = "";
        delete formIsError.phone;
    }
    else {
        let message = '';
        if (validationByPhone.validation(value).errors.phone != undefined) {
            message = 'Phone is not valid';
            userContactEll.classList.add('form__control-error');
        }
        if (validationByPhone.validation(value).errors.required != undefined) {
            message = 'Phone is required';
            userContactEll.classList.add('form__control-error');
        }
        formIsError.phone = true;
        delete formIsError.email;
        userContactMessageId.innerHTML = `<div class="message message-error message-show">${message}</div>`;
    }
}
/**
 * Сheck obj is empty
 *
 * @param obj
 */
function isEmpty(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
/**
 * Form submit
 */
function submit(event, data) {
    if (isEmpty(formIsError)) {
        let XHR = new XMLHttpRequest();
        let formData = new FormData();
        formData.append('name', data.name);
        formData.append('find', data.find);
        formData.append('receive', data.receive);
        formData.append('userContact', data.userContact);
        // Define what happens on successful data submission
        XHR.addEventListener('load', function () {
            // Yeah! Data sent and response loaded.
        });
        // Define what happens in case of error
        XHR.addEventListener('error', function () {
            // Oops! Something went wrong.
        });
        XHR.open("POST", sendDataUrl);
        XHR.send(formData);
    }
}
/**
 * Event by name field
 *
 * @param event
 */
function inputNameEvent(event) {
    let inputValue = event.target.value;
    nameValidation(inputValue);
}
/**
 * Event by user contact field
 *
 * @param event
 */
function inputUserContactEvent(event) {
    let inputValue = event.target.value, changeReceive = document.getElementById('receive').value;
    if (changeReceive == 'email') {
        emailValidation(inputValue);
    }
    else {
        phoneValidation(inputValue);
    }
}
// Init event form
let changeInputName = document.getElementById('name');
let changeReceive = document.getElementById('receive');
let changeUserContact = document.getElementById('userContact');
// Init event click by name field
changeInputName.addEventListener('click', (event) => {
    inputNameEvent(event);
});
// Init event change by name field
changeInputName.addEventListener('change', (event) => {
    inputNameEvent(event);
});
// Init event change by receive field
changeReceive.addEventListener('change', (event) => {
    let contactLabelText = document.getElementById('contactLabelText'), label = 'email';
    changeUserContact.value = '';
    if (changeReceive.value == 'sms') {
        label = `phone number`;
    }
    // contactLabelText.innerHTML = label;
    contactLabelText.innerHTML = `Share your ${label} with us`;
    inputUserContactEvent(event);
});
// Init event click by userContact field
changeUserContact.addEventListener('click', (event) => {
    inputUserContactEvent(event);
});
// Init event change by userContact field
changeUserContact.addEventListener('change', (event) => {
    inputUserContactEvent(event);
});
let formSubmit = document.getElementById('submit');
// Init event change by userContact field
formSubmit.addEventListener('click', (event) => {
    event.preventDefault();
    let data = {
        'name': document.getElementById('name').value,
        'find': document.getElementById('find').value,
        'receive': document.getElementById('receive').value,
        'userContact': document.getElementById('userContact').value
    };
    submit(event, data);
});
},{"./EmailValidator":2,"./NameValidator":3,"./PhoneValidator":4}]},{},[5])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvZGV2L2pzL1R5cGVTY3JpcHQvQmFzZVZhbGlkYXRvci50cyIsImFwcC9kZXYvanMvVHlwZVNjcmlwdC9FbWFpbFZhbGlkYXRvci50cyIsImFwcC9kZXYvanMvVHlwZVNjcmlwdC9OYW1lVmFsaWRhdG9yLnRzIiwiYXBwL2Rldi9qcy9UeXBlU2NyaXB0L1Bob25lVmFsaWRhdG9yLnRzIiwiYXBwL2Rldi9qcy9UeXBlU2NyaXB0L21haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0VhLFFBQUEsY0FBYyxHQUFHLGNBQWMsQ0FBQztBQUNoQyxRQUFBLFlBQVksR0FBRyxvQkFBb0IsQ0FBQztBQUNwQyxRQUFBLFdBQVcsR0FBRyx3SkFBd0osQ0FBQztBQUN2SyxRQUFBLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQztBQUUvQyxNQUFhLGFBQWE7SUFDdEI7Ozs7T0FJRztJQUNILFVBQVUsQ0FBQyxDQUFTO1FBRWhCLE9BQU8sc0JBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxZQUFZLENBQUMsQ0FBUztRQUVsQixPQUFPLG9CQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsVUFBVSxDQUFDLENBQVM7UUFFaEIsT0FBTyxtQkFBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILFdBQVcsQ0FBQyxDQUFTO1FBRWpCLE9BQU8sbUJBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxPQUFPLENBQUMsR0FBVztRQUNmLEtBQUksSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFO1lBQ2hCLElBQUcsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUM7Z0JBQ3RCLE9BQU8sS0FBSyxDQUFDO1NBQ3BCO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxhQUFhLENBQUMsQ0FBUztRQUVuQixPQUFPO1lBQ0gsUUFBUSxFQUFFLENBQUM7U0FDZCxDQUFDO0lBQ04sQ0FBQztDQUNKO0FBbEVELHNDQWtFQzs7OztBQ3pFRCxtREFBZ0Q7QUFFaEQsTUFBYSxjQUFlLFNBQVEsNkJBQWE7SUFDN0M7Ozs7T0FJRztJQUNILFVBQVUsQ0FBQyxLQUFhO1FBQ3BCLElBQUksTUFBTSxHQUFRLEVBQUUsQ0FBQztRQUNyQixJQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN4QixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUMxQjtRQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3hCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFFdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKO0FBckJELHdDQXFCQzs7OztBQ3ZCRCxtREFBZ0Q7QUFFaEQsTUFBYSxhQUFjLFNBQVEsNkJBQWE7SUFDNUM7Ozs7T0FJRztJQUNILFVBQVUsQ0FBQyxLQUFhO1FBQ3BCLElBQUksTUFBTSxHQUFRLEVBQUUsQ0FBQztRQUNyQixJQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN4QixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUMxQjtRQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzFCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3hCO1FBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFFdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKO0FBckJELHNDQXFCQzs7OztBQ3ZCRCxtREFBZ0Q7QUFFaEQsTUFBYSxjQUFlLFNBQVEsNkJBQWE7SUFDN0M7Ozs7T0FJRztJQUNILFVBQVUsQ0FBQyxLQUFhO1FBQ3BCLElBQUksTUFBTSxHQUFRLEVBQUUsQ0FBQztRQUNyQixJQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN4QixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUMxQjtRQUVELElBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3pCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBRUQsSUFBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFFdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKO0FBdkJELHdDQXVCQzs7OztBQ3pCRCxtREFBZ0Q7QUFDaEQscURBQWtEO0FBQ2xELHFEQUFrRDtBQUVsRCxNQUFNLGdCQUFnQixHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO0FBQzdDLE1BQU0saUJBQWlCLEdBQUcsSUFBSSwrQkFBYyxFQUFFLENBQUM7QUFDL0MsTUFBTSxpQkFBaUIsR0FBRyxJQUFJLCtCQUFjLEVBQUUsQ0FBQztBQUUvQyxNQUFNLFdBQVcsR0FBUTtJQUNyQixNQUFNLEVBQUUsSUFBSTtJQUNaLE9BQU8sRUFBRSxJQUFJO0NBQ2hCLENBQUM7QUFDRixNQUFNLFdBQVcsR0FBRyxHQUFHLENBQUM7QUFFeEI7Ozs7R0FJRztBQUNILFNBQVMsY0FBYyxDQUFDLEtBQUs7SUFFekIsSUFBSSxPQUFPLEdBQXNCLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFFLEVBQzdELGFBQWEsR0FBc0IsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUUsQ0FBQztJQUUvRSxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ2hELE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFFbEQsSUFBRyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLFNBQVMsRUFBRTtRQUN2RCxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQy9DLGFBQWEsQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQzdCLE9BQU8sV0FBVyxDQUFDLElBQUksQ0FBQztLQUMzQjtTQUFNO1FBQ0gsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUM3QyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBRWxELElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFHLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLFNBQVMsRUFBRTtZQUM5RCxPQUFPLEdBQUcsb0JBQW9CLENBQUM7U0FDbEM7UUFDRCxJQUFHLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFJLFNBQVMsRUFBRTtZQUNoRSxPQUFPLEdBQUcsa0JBQWtCLENBQUM7U0FDaEM7UUFDRCxXQUFXLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUV4QixhQUFhLENBQUMsU0FBUyxHQUFHLG1EQUFtRCxPQUFPLFFBQVEsQ0FBQztLQUNoRztBQUNMLENBQUM7QUFFRDs7OztHQUlHO0FBQ0gsU0FBUyxlQUFlLENBQUMsS0FBSztJQUUxQixJQUFJLGNBQWMsR0FBc0IsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUUsRUFDM0Usb0JBQW9CLEdBQXNCLFFBQVEsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUUsQ0FBQztJQUU3RixjQUFjLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3ZELGNBQWMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFFekQsSUFBRyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLFNBQVMsRUFBRTtRQUN4RCxjQUFjLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQ3ZELGNBQWMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDdEQsb0JBQW9CLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQyxPQUFPLFdBQVcsQ0FBQyxLQUFLLENBQUM7S0FDNUI7U0FBTTtRQUNILElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFHLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLFNBQVMsRUFBRTtZQUM5RCxPQUFPLEdBQUcsb0JBQW9CLENBQUM7WUFDL0IsY0FBYyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztTQUN2RDtRQUVELElBQUcsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksU0FBUyxFQUFFO1lBQ2pFLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQztZQUM5QixjQUFjLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1NBQ3ZEO1FBQ0QsV0FBVyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDekIsT0FBTyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBRXpCLG9CQUFvQixDQUFDLFNBQVMsR0FBRyxtREFBbUQsT0FBTyxRQUFRLENBQUM7S0FDdkc7QUFDTCxDQUFDO0FBRUQ7Ozs7R0FJRztBQUNILFNBQVMsZUFBZSxDQUFDLEtBQUs7SUFFMUIsSUFBSSxjQUFjLEdBQXNCLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFFLEVBQzNFLG9CQUFvQixHQUFzQixRQUFRLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFFLENBQUM7SUFFN0YsY0FBYyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUN2RCxjQUFjLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO0lBRXpELElBQUcsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxTQUFTLEVBQUU7UUFDeEQsY0FBYyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUN2RCxjQUFjLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQ3RELG9CQUFvQixDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDcEMsT0FBTyxXQUFXLENBQUMsS0FBSyxDQUFDO0tBQzVCO1NBQU07UUFDSCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBRyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxTQUFTLEVBQUU7WUFDOUQsT0FBTyxHQUFHLG9CQUFvQixDQUFDO1lBQy9CLGNBQWMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7U0FDdkQ7UUFDRCxJQUFHLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFJLFNBQVMsRUFBRTtZQUNqRSxPQUFPLEdBQUcsbUJBQW1CLENBQUM7WUFDOUIsY0FBYyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztTQUN2RDtRQUNELFdBQVcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLE9BQU8sV0FBVyxDQUFDLEtBQUssQ0FBQztRQUV6QixvQkFBb0IsQ0FBQyxTQUFTLEdBQUcsbURBQW1ELE9BQU8sUUFBUSxDQUFDO0tBQ3ZHO0FBQ0wsQ0FBQztBQUVEOzs7O0dBSUc7QUFDSCxTQUFTLE9BQU8sQ0FBQyxHQUFXO0lBRXhCLEtBQUksSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFO1FBQ2hCLElBQUcsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUM7WUFDdEIsT0FBTyxLQUFLLENBQUM7S0FDcEI7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDO0FBRUQ7O0dBRUc7QUFDSCxTQUFTLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSTtJQUV2QixJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFBRTtRQUN0QixJQUFJLEdBQUcsR0FBRyxJQUFJLGNBQWMsRUFBRSxDQUFDO1FBQy9CLElBQUksUUFBUSxHQUFJLElBQUksUUFBUSxFQUFFLENBQUM7UUFDL0IsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRWpELG9EQUFvRDtRQUNwRCxHQUFHLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1lBQ3pCLHVDQUF1QztRQUMzQyxDQUFDLENBQUMsQ0FBQztRQUVILHVDQUF1QztRQUN2QyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO1lBQzFCLDhCQUE4QjtRQUNsQyxDQUFDLENBQUMsQ0FBQztRQUVILEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQzlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDdEI7QUFDTCxDQUFDO0FBRUQ7Ozs7R0FJRztBQUNILFNBQVMsY0FBYyxDQUFDLEtBQUs7SUFFekIsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDcEMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQy9CLENBQUM7QUFFRDs7OztHQUlHO0FBQ0gsU0FBUyxxQkFBcUIsQ0FBQyxLQUFLO0lBRWhDLElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUMvQixhQUFhLEdBQXNCLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFFLENBQUMsS0FBSyxDQUFDO0lBRWpGLElBQUcsYUFBYSxJQUFJLE9BQU8sRUFBRTtRQUN6QixlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7S0FDL0I7U0FBTTtRQUNILGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztLQUMvQjtBQUNMLENBQUM7QUFFRCxrQkFBa0I7QUFDbEIsSUFBSSxlQUFlLEdBQXNCLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFFLENBQUM7QUFDMUUsSUFBSSxhQUFhLEdBQXNCLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFFLENBQUM7QUFDM0UsSUFBSSxpQkFBaUIsR0FBc0IsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUUsQ0FBQztBQUVuRixpQ0FBaUM7QUFDakMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO0lBQ2hELGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUMxQixDQUFDLENBQUMsQ0FBQztBQUVILGtDQUFrQztBQUNsQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7SUFDakQsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQzFCLENBQUMsQ0FBQyxDQUFDO0FBRUgscUNBQXFDO0FBQ3JDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtJQUMvQyxJQUFJLGdCQUFnQixHQUFzQixRQUFRLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFFLEVBQ2xGLEtBQUssR0FBRyxPQUFPLENBQUM7SUFFcEIsaUJBQWlCLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUM3QixJQUFHLGFBQWEsQ0FBQyxLQUFLLElBQUksS0FBSyxFQUFFO1FBQzdCLEtBQUssR0FBRyxjQUFjLENBQUM7S0FDMUI7SUFDRCxzQ0FBc0M7SUFDdEMsZ0JBQWdCLENBQUMsU0FBUyxHQUFHLGNBQWMsS0FBSyxVQUFVLENBQUM7SUFFM0QscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDakMsQ0FBQyxDQUFDLENBQUM7QUFFSCx3Q0FBd0M7QUFDeEMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7SUFDbEQscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDakMsQ0FBQyxDQUFDLENBQUM7QUFFSCx5Q0FBeUM7QUFDekMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7SUFDbkQscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDakMsQ0FBQyxDQUFDLENBQUM7QUFFSCxJQUFJLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQ25ELHlDQUF5QztBQUN6QyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7SUFDM0MsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3ZCLElBQUksSUFBSSxHQUFHO1FBQ1AsTUFBTSxFQUFxQixRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBRSxDQUFDLEtBQUs7UUFDakUsTUFBTSxFQUFxQixRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBRSxDQUFDLEtBQUs7UUFDakUsU0FBUyxFQUFxQixRQUFRLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBRSxDQUFDLEtBQUs7UUFDdkUsYUFBYSxFQUFxQixRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBRSxDQUFDLEtBQUs7S0FDbEYsQ0FBQztJQUNGLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7QUFDeEIsQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCJpbXBvcnQgeyBTdHJpbmdWYWxpZGF0b3IgfSBmcm9tIFwiLi9WYWxpZGF0aW9uXCI7XHJcblxyXG5leHBvcnQgY29uc3QgcmVxdWlyZWRSZWdleHAgPSAvXig/IVxccyokKS4rJC87XHJcbmV4cG9ydCBjb25zdCBzdHJpbmdSZWdleHAgPSAvXlthLXpBLVrQsC3Rj9CQLdCvIF0rJC87XHJcbmV4cG9ydCBjb25zdCBlbWFpbFJlZ2V4cCA9IC9eKChbXjw+KClcXFtcXF1cXFxcLiw7Olxcc0BcIl0rKFxcLltePD4oKVxcW1xcXVxcXFwuLDs6XFxzQFwiXSspKil8KFwiLitcIikpQCgoXFxbWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfV0pfCgoW2EtekEtWlxcLTAtOV0rXFwuKStbYS16QS1aXXsyLH0pKSQvO1xyXG5leHBvcnQgY29uc3QgcGhvbmVSZWdleHAgPSAvXFwrXFwoXFxkezN9XFwpXFxkezl9JC87XHJcblxyXG5leHBvcnQgY2xhc3MgQmFzZVZhbGlkYXRvciBpbXBsZW1lbnRzIFN0cmluZ1ZhbGlkYXRvciB7XHJcbiAgICAvKipcclxuICAgICAqINChaGVjayBmaWVsZCByZXF1aXJlZFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBzXHJcbiAgICAgKi9cclxuICAgIGlzUmVxdWlyZWQoczogc3RyaW5nKSB7XHJcblxyXG4gICAgICAgIHJldHVybiByZXF1aXJlZFJlZ2V4cC50ZXN0KHMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog0KFoZWNrIGZpZWxkIHR5cGUgc3RyaW5nXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHNcclxuICAgICAqL1xyXG4gICAgaXNUeXBlU3RyaW5nKHM6IHN0cmluZykge1xyXG5cclxuICAgICAgICByZXR1cm4gc3RyaW5nUmVnZXhwLnRlc3Qocyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDQoWhlY2sgZmllbGQgdHlwZSBtYWlsXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHNcclxuICAgICAqL1xyXG4gICAgaXNUeXBlTWFpbChzOiBzdHJpbmcpIHtcclxuXHJcbiAgICAgICAgcmV0dXJuIGVtYWlsUmVnZXhwLnRlc3Qocyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDQoWhlY2sgZmllbGQgdHlwZSBwaG9uZVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBzXHJcbiAgICAgKi9cclxuICAgIGlzVHlwZVBob25lKHM6IHN0cmluZykge1xyXG5cclxuICAgICAgICByZXR1cm4gcGhvbmVSZWdleHAudGVzdChzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqINChaGVjayBvYmogaXMgZW1wdHlcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gb2JqXHJcbiAgICAgKi9cclxuICAgIGlzRW1wdHkob2JqOiBvYmplY3QpIHtcclxuICAgICAgICBmb3IobGV0IGtleSBpbiBvYmopIHtcclxuICAgICAgICAgICAgaWYob2JqLmhhc093blByb3BlcnR5KGtleSkpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIHJlc3BvbnNlIGVycm9yXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGVcclxuICAgICAqL1xyXG4gICAgcmVzcG9uc2VFcnJvcihlOiBvYmplY3QpIHtcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgJ2Vycm9ycyc6IGVcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQmFzZVZhbGlkYXRvciB9IGZyb20gXCIuL0Jhc2VWYWxpZGF0b3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBFbWFpbFZhbGlkYXRvciBleHRlbmRzIEJhc2VWYWxpZGF0b3Ige1xyXG4gICAgLyoqXHJcbiAgICAgKiBWYWxpZGF0aW9uIGVtYWlsIGZpZWxkXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHZhbHVlXHJcbiAgICAgKi9cclxuICAgIHZhbGlkYXRpb24odmFsdWU6IHN0cmluZyk6YW55IHtcclxuICAgICAgICBsZXQgZXJyb3JzOiBhbnkgPSB7fTtcclxuICAgICAgICBpZighdGhpcy5pc1JlcXVpcmVkKHZhbHVlKSkge1xyXG4gICAgICAgICAgICBlcnJvcnMucmVxdWlyZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZighdGhpcy5pc1R5cGVNYWlsKHZhbHVlKSkge1xyXG4gICAgICAgICAgICBlcnJvcnMuZW1haWwgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZighdGhpcy5pc0VtcHR5KGVycm9ycykpIHtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnJlc3BvbnNlRXJyb3IoZXJyb3JzKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQmFzZVZhbGlkYXRvciB9IGZyb20gXCIuL0Jhc2VWYWxpZGF0b3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBOYW1lVmFsaWRhdG9yIGV4dGVuZHMgQmFzZVZhbGlkYXRvciB7XHJcbiAgICAvKipcclxuICAgICAqIFZhbGlkYXRpb24gbmFtZSBmaWVsZFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAgICovXHJcbiAgICB2YWxpZGF0aW9uKHZhbHVlOiBzdHJpbmcpOmFueSB7XHJcbiAgICAgICAgbGV0IGVycm9yczogYW55ID0ge307XHJcbiAgICAgICAgaWYoIXRoaXMuaXNSZXF1aXJlZCh2YWx1ZSkpIHtcclxuICAgICAgICAgICAgZXJyb3JzLnJlcXVpcmVkID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoIXRoaXMuaXNUeXBlU3RyaW5nKHZhbHVlKSkge1xyXG4gICAgICAgICAgICBlcnJvcnMuc3RyaW5nID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoIXRoaXMuaXNFbXB0eShlcnJvcnMpKSB7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yZXNwb25zZUVycm9yKGVycm9ycyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufSIsImltcG9ydCB7IEJhc2VWYWxpZGF0b3IgfSBmcm9tIFwiLi9CYXNlVmFsaWRhdG9yXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgUGhvbmVWYWxpZGF0b3IgZXh0ZW5kcyBCYXNlVmFsaWRhdG9yIHtcclxuICAgIC8qKlxyXG4gICAgICogVmFsaWRhdGlvbiBwaG9uZSBmaWVsZFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAgICovXHJcbiAgICB2YWxpZGF0aW9uKHZhbHVlOiBzdHJpbmcpOmFueSB7XHJcbiAgICAgICAgbGV0IGVycm9yczogYW55ID0ge307XHJcbiAgICAgICAgaWYoIXRoaXMuaXNSZXF1aXJlZCh2YWx1ZSkpIHtcclxuICAgICAgICAgICAgZXJyb3JzLnJlcXVpcmVkID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKCF0aGlzLmlzVHlwZVBob25lKHZhbHVlKSkge1xyXG4gICAgICAgICAgICBlcnJvcnMucGhvbmUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYoIXRoaXMuaXNFbXB0eShlcnJvcnMpKSB7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yZXNwb25zZUVycm9yKGVycm9ycyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufSIsImltcG9ydCB7IE5hbWVWYWxpZGF0b3IgfSBmcm9tIFwiLi9OYW1lVmFsaWRhdG9yXCI7XHJcbmltcG9ydCB7IEVtYWlsVmFsaWRhdG9yIH0gZnJvbSBcIi4vRW1haWxWYWxpZGF0b3JcIjtcclxuaW1wb3J0IHsgUGhvbmVWYWxpZGF0b3IgfSBmcm9tIFwiLi9QaG9uZVZhbGlkYXRvclwiO1xyXG5cclxuY29uc3QgdmFsaWRhdGlvbkJ5TmFtZSA9IG5ldyBOYW1lVmFsaWRhdG9yKCk7XHJcbmNvbnN0IHZhbGlkYXRpb25CeUVtYWlsID0gbmV3IEVtYWlsVmFsaWRhdG9yKCk7XHJcbmNvbnN0IHZhbGlkYXRpb25CeVBob25lID0gbmV3IFBob25lVmFsaWRhdG9yKCk7XHJcblxyXG5jb25zdCBmb3JtSXNFcnJvcjogYW55ID0ge1xyXG4gICAgJ25hbWUnOiB0cnVlLFxyXG4gICAgJ2VtYWlsJzogdHJ1ZVxyXG59O1xyXG5jb25zdCBzZW5kRGF0YVVybCA9ICcvJztcclxuXHJcbi8qKlxyXG4gKiBGb3JtIGlucHV0IG5hbWUgdmFsaWRhdGlvblxyXG4gKlxyXG4gKiBAcGFyYW0gdmFsdWVcclxuICovXHJcbmZ1bmN0aW9uIG5hbWVWYWxpZGF0aW9uKHZhbHVlKVxyXG57XHJcbiAgICBsZXQgbmFtZUVsbCA9ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbmFtZScpKSxcclxuICAgICAgICBuYW1lTWVzc2FnZUlkID0gKDxIVE1MSW5wdXRFbGVtZW50PmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCduYW1lTWVzc2FnZScpKTtcclxuXHJcbiAgICBuYW1lRWxsLmNsYXNzTGlzdC5yZW1vdmUoJ2Zvcm1fX2NvbnRyb2wtZXJyb3InKTtcclxuICAgIG5hbWVFbGwuY2xhc3NMaXN0LnJlbW92ZSgnZm9ybV9fY29udHJvbC1zdWNjZXNzJyk7XHJcblxyXG4gICAgaWYodmFsaWRhdGlvbkJ5TmFtZS52YWxpZGF0aW9uKHZhbHVlKS5lcnJvcnMgPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgbmFtZUVsbC5jbGFzc0xpc3QuYWRkKCdmb3JtX19jb250cm9sLXN1Y2Nlc3MnKTtcclxuICAgICAgICBuYW1lTWVzc2FnZUlkLmlubmVySFRNTCA9IFwiXCI7XHJcbiAgICAgICAgZGVsZXRlIGZvcm1Jc0Vycm9yLm5hbWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIG5hbWVFbGwuY2xhc3NMaXN0LmFkZCgnZm9ybV9fY29udHJvbC1lcnJvcicpO1xyXG4gICAgICAgIG5hbWVFbGwuY2xhc3NMaXN0LnJlbW92ZSgnZm9ybV9fY29udHJvbC1zdWNjZXNzJyk7XHJcblxyXG4gICAgICAgIGxldCBtZXNzYWdlID0gJyc7XHJcbiAgICAgICAgaWYodmFsaWRhdGlvbkJ5TmFtZS52YWxpZGF0aW9uKHZhbHVlKS5lcnJvcnMuc3RyaW5nICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gJ05hbWUgaXMgbm90IHN0cmluZyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKHZhbGlkYXRpb25CeU5hbWUudmFsaWRhdGlvbih2YWx1ZSkuZXJyb3JzLnJlcXVpcmVkICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gJ05hbWUgaXMgcmVxdWlyZWQnO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtSXNFcnJvci5uYW1lID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgbmFtZU1lc3NhZ2VJZC5pbm5lckhUTUwgPSBgPGRpdiBjbGFzcz1cIm1lc3NhZ2UgbWVzc2FnZS1lcnJvciBtZXNzYWdlLXNob3dcIj4ke21lc3NhZ2V9PC9kaXY+YDtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEZvcm0gaW5wdXQgZW1haWwgdmFsaWRhdGlvblxyXG4gKlxyXG4gKiBAcGFyYW0gdmFsdWVcclxuICovXHJcbmZ1bmN0aW9uIGVtYWlsVmFsaWRhdGlvbih2YWx1ZSlcclxue1xyXG4gICAgbGV0IHVzZXJDb250YWN0RWxsID0gKDxIVE1MSW5wdXRFbGVtZW50PmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd1c2VyQ29udGFjdCcpKSxcclxuICAgICAgICB1c2VyQ29udGFjdE1lc3NhZ2VJZCA9ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndXNlckNvbnRhY3RNZXNzYWdlJykpO1xyXG5cclxuICAgIHVzZXJDb250YWN0RWxsLmNsYXNzTGlzdC5yZW1vdmUoJ2Zvcm1fX2NvbnRyb2wtZXJyb3InKTtcclxuICAgIHVzZXJDb250YWN0RWxsLmNsYXNzTGlzdC5yZW1vdmUoJ2Zvcm1fX2NvbnRyb2wtc3VjY2VzcycpO1xyXG5cclxuICAgIGlmKHZhbGlkYXRpb25CeUVtYWlsLnZhbGlkYXRpb24odmFsdWUpLmVycm9ycyA9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICB1c2VyQ29udGFjdEVsbC5jbGFzc0xpc3QucmVtb3ZlKCdmb3JtX19jb250cm9sLWVycm9yJyk7XHJcbiAgICAgICAgdXNlckNvbnRhY3RFbGwuY2xhc3NMaXN0LmFkZCgnZm9ybV9fY29udHJvbC1zdWNjZXNzJyk7XHJcbiAgICAgICAgdXNlckNvbnRhY3RNZXNzYWdlSWQuaW5uZXJIVE1MID0gXCJcIjtcclxuICAgICAgICBkZWxldGUgZm9ybUlzRXJyb3IuZW1haWw7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGxldCBtZXNzYWdlID0gJyc7XHJcbiAgICAgICAgaWYodmFsaWRhdGlvbkJ5RW1haWwudmFsaWRhdGlvbih2YWx1ZSkuZXJyb3JzLmVtYWlsICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gJ0VtYWlsIGlzIG5vdCB2YWxpZCc7XHJcbiAgICAgICAgICAgIHVzZXJDb250YWN0RWxsLmNsYXNzTGlzdC5hZGQoJ2Zvcm1fX2NvbnRyb2wtZXJyb3InKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKHZhbGlkYXRpb25CeUVtYWlsLnZhbGlkYXRpb24odmFsdWUpLmVycm9ycy5yZXF1aXJlZCAhPSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9ICdFbWFpbCBpcyByZXF1aXJlZCc7XHJcbiAgICAgICAgICAgIHVzZXJDb250YWN0RWxsLmNsYXNzTGlzdC5hZGQoJ2Zvcm1fX2NvbnRyb2wtZXJyb3InKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9ybUlzRXJyb3IuZW1haWwgPSB0cnVlO1xyXG4gICAgICAgIGRlbGV0ZSBmb3JtSXNFcnJvci5waG9uZTtcclxuXHJcbiAgICAgICAgdXNlckNvbnRhY3RNZXNzYWdlSWQuaW5uZXJIVE1MID0gYDxkaXYgY2xhc3M9XCJtZXNzYWdlIG1lc3NhZ2UtZXJyb3IgbWVzc2FnZS1zaG93XCI+JHttZXNzYWdlfTwvZGl2PmA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBGb3JtIGlucHV0IHBob25lIHZhbGlkYXRpb25cclxuICpcclxuICogQHBhcmFtIHZhbHVlXHJcbiAqL1xyXG5mdW5jdGlvbiBwaG9uZVZhbGlkYXRpb24odmFsdWUpXHJcbntcclxuICAgIGxldCB1c2VyQ29udGFjdEVsbCA9ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndXNlckNvbnRhY3QnKSksXHJcbiAgICAgICAgdXNlckNvbnRhY3RNZXNzYWdlSWQgPSAoPEhUTUxJbnB1dEVsZW1lbnQ+ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3VzZXJDb250YWN0TWVzc2FnZScpKTtcclxuXHJcbiAgICB1c2VyQ29udGFjdEVsbC5jbGFzc0xpc3QucmVtb3ZlKCdmb3JtX19jb250cm9sLWVycm9yJyk7XHJcbiAgICB1c2VyQ29udGFjdEVsbC5jbGFzc0xpc3QucmVtb3ZlKCdmb3JtX19jb250cm9sLXN1Y2Nlc3MnKTtcclxuXHJcbiAgICBpZih2YWxpZGF0aW9uQnlQaG9uZS52YWxpZGF0aW9uKHZhbHVlKS5lcnJvcnMgPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgdXNlckNvbnRhY3RFbGwuY2xhc3NMaXN0LnJlbW92ZSgnZm9ybV9fY29udHJvbC1lcnJvcicpO1xyXG4gICAgICAgIHVzZXJDb250YWN0RWxsLmNsYXNzTGlzdC5hZGQoJ2Zvcm1fX2NvbnRyb2wtc3VjY2VzcycpO1xyXG4gICAgICAgIHVzZXJDb250YWN0TWVzc2FnZUlkLmlubmVySFRNTCA9IFwiXCI7XHJcbiAgICAgICAgZGVsZXRlIGZvcm1Jc0Vycm9yLnBob25lO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBsZXQgbWVzc2FnZSA9ICcnO1xyXG4gICAgICAgIGlmKHZhbGlkYXRpb25CeVBob25lLnZhbGlkYXRpb24odmFsdWUpLmVycm9ycy5waG9uZSAhPSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9ICdQaG9uZSBpcyBub3QgdmFsaWQnO1xyXG4gICAgICAgICAgICB1c2VyQ29udGFjdEVsbC5jbGFzc0xpc3QuYWRkKCdmb3JtX19jb250cm9sLWVycm9yJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKHZhbGlkYXRpb25CeVBob25lLnZhbGlkYXRpb24odmFsdWUpLmVycm9ycy5yZXF1aXJlZCAhPSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9ICdQaG9uZSBpcyByZXF1aXJlZCc7XHJcbiAgICAgICAgICAgIHVzZXJDb250YWN0RWxsLmNsYXNzTGlzdC5hZGQoJ2Zvcm1fX2NvbnRyb2wtZXJyb3InKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9ybUlzRXJyb3IucGhvbmUgPSB0cnVlO1xyXG4gICAgICAgIGRlbGV0ZSBmb3JtSXNFcnJvci5lbWFpbDtcclxuXHJcbiAgICAgICAgdXNlckNvbnRhY3RNZXNzYWdlSWQuaW5uZXJIVE1MID0gYDxkaXYgY2xhc3M9XCJtZXNzYWdlIG1lc3NhZ2UtZXJyb3IgbWVzc2FnZS1zaG93XCI+JHttZXNzYWdlfTwvZGl2PmA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDQoWhlY2sgb2JqIGlzIGVtcHR5XHJcbiAqXHJcbiAqIEBwYXJhbSBvYmpcclxuICovXHJcbmZ1bmN0aW9uIGlzRW1wdHkob2JqOiBvYmplY3QpXHJcbntcclxuICAgIGZvcihsZXQga2V5IGluIG9iaikge1xyXG4gICAgICAgIGlmKG9iai5oYXNPd25Qcm9wZXJ0eShrZXkpKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBGb3JtIHN1Ym1pdFxyXG4gKi9cclxuZnVuY3Rpb24gc3VibWl0KGV2ZW50LCBkYXRhKVxyXG57XHJcbiAgICBpZiAoaXNFbXB0eShmb3JtSXNFcnJvcikpIHtcclxuICAgICAgICBsZXQgWEhSID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgICAgICAgbGV0IGZvcm1EYXRhICA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgIGZvcm1EYXRhLmFwcGVuZCgnbmFtZScsIGRhdGEubmFtZSk7XHJcbiAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdmaW5kJywgZGF0YS5maW5kKTtcclxuICAgICAgICBmb3JtRGF0YS5hcHBlbmQoJ3JlY2VpdmUnLCBkYXRhLnJlY2VpdmUpO1xyXG4gICAgICAgIGZvcm1EYXRhLmFwcGVuZCgndXNlckNvbnRhY3QnLCBkYXRhLnVzZXJDb250YWN0KTtcclxuXHJcbiAgICAgICAgLy8gRGVmaW5lIHdoYXQgaGFwcGVucyBvbiBzdWNjZXNzZnVsIGRhdGEgc3VibWlzc2lvblxyXG4gICAgICAgIFhIUi5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIC8vIFllYWghIERhdGEgc2VudCBhbmQgcmVzcG9uc2UgbG9hZGVkLlxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBEZWZpbmUgd2hhdCBoYXBwZW5zIGluIGNhc2Ugb2YgZXJyb3JcclxuICAgICAgICBYSFIuYWRkRXZlbnRMaXN0ZW5lcignZXJyb3InLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgLy8gT29wcyEgU29tZXRoaW5nIHdlbnQgd3JvbmcuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIFhIUi5vcGVuKFwiUE9TVFwiLCBzZW5kRGF0YVVybCk7XHJcbiAgICAgICAgWEhSLnNlbmQoZm9ybURhdGEpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogRXZlbnQgYnkgbmFtZSBmaWVsZFxyXG4gKlxyXG4gKiBAcGFyYW0gZXZlbnRcclxuICovXHJcbmZ1bmN0aW9uIGlucHV0TmFtZUV2ZW50KGV2ZW50KVxyXG57XHJcbiAgICBsZXQgaW5wdXRWYWx1ZSA9IGV2ZW50LnRhcmdldC52YWx1ZTtcclxuICAgIG5hbWVWYWxpZGF0aW9uKGlucHV0VmFsdWUpO1xyXG59XHJcblxyXG4vKipcclxuICogRXZlbnQgYnkgdXNlciBjb250YWN0IGZpZWxkXHJcbiAqXHJcbiAqIEBwYXJhbSBldmVudFxyXG4gKi9cclxuZnVuY3Rpb24gaW5wdXRVc2VyQ29udGFjdEV2ZW50KGV2ZW50KVxyXG57XHJcbiAgICBsZXQgaW5wdXRWYWx1ZSA9IGV2ZW50LnRhcmdldC52YWx1ZSxcclxuICAgICAgICBjaGFuZ2VSZWNlaXZlID0gKDxIVE1MSW5wdXRFbGVtZW50PmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyZWNlaXZlJykpLnZhbHVlO1xyXG5cclxuICAgIGlmKGNoYW5nZVJlY2VpdmUgPT0gJ2VtYWlsJykge1xyXG4gICAgICAgIGVtYWlsVmFsaWRhdGlvbihpbnB1dFZhbHVlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcGhvbmVWYWxpZGF0aW9uKGlucHV0VmFsdWUpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vLyBJbml0IGV2ZW50IGZvcm1cclxubGV0IGNoYW5nZUlucHV0TmFtZSA9ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbmFtZScpKTtcclxubGV0IGNoYW5nZVJlY2VpdmUgPSAoPEhUTUxJbnB1dEVsZW1lbnQ+ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3JlY2VpdmUnKSk7XHJcbmxldCBjaGFuZ2VVc2VyQ29udGFjdCA9ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndXNlckNvbnRhY3QnKSk7XHJcblxyXG4vLyBJbml0IGV2ZW50IGNsaWNrIGJ5IG5hbWUgZmllbGRcclxuY2hhbmdlSW5wdXROYW1lLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGV2ZW50KSA9PiB7XHJcbiAgICBpbnB1dE5hbWVFdmVudChldmVudCk7XHJcbn0pO1xyXG5cclxuLy8gSW5pdCBldmVudCBjaGFuZ2UgYnkgbmFtZSBmaWVsZFxyXG5jaGFuZ2VJbnB1dE5hbWUuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgKGV2ZW50KSA9PiB7XHJcbiAgICBpbnB1dE5hbWVFdmVudChldmVudCk7XHJcbn0pO1xyXG5cclxuLy8gSW5pdCBldmVudCBjaGFuZ2UgYnkgcmVjZWl2ZSBmaWVsZFxyXG5jaGFuZ2VSZWNlaXZlLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIChldmVudCkgPT4ge1xyXG4gICAgbGV0IGNvbnRhY3RMYWJlbFRleHQgPSAoPEhUTUxJbnB1dEVsZW1lbnQ+ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NvbnRhY3RMYWJlbFRleHQnKSksXHJcbiAgICAgICAgbGFiZWwgPSAnZW1haWwnO1xyXG5cclxuICAgIGNoYW5nZVVzZXJDb250YWN0LnZhbHVlID0gJyc7XHJcbiAgICBpZihjaGFuZ2VSZWNlaXZlLnZhbHVlID09ICdzbXMnKSB7XHJcbiAgICAgICAgbGFiZWwgPSBgcGhvbmUgbnVtYmVyYDtcclxuICAgIH1cclxuICAgIC8vIGNvbnRhY3RMYWJlbFRleHQuaW5uZXJIVE1MID0gbGFiZWw7XHJcbiAgICBjb250YWN0TGFiZWxUZXh0LmlubmVySFRNTCA9IGBTaGFyZSB5b3VyICR7bGFiZWx9IHdpdGggdXNgO1xyXG5cclxuICAgIGlucHV0VXNlckNvbnRhY3RFdmVudChldmVudCk7XHJcbn0pO1xyXG5cclxuLy8gSW5pdCBldmVudCBjbGljayBieSB1c2VyQ29udGFjdCBmaWVsZFxyXG5jaGFuZ2VVc2VyQ29udGFjdC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldmVudCkgPT4ge1xyXG4gICAgaW5wdXRVc2VyQ29udGFjdEV2ZW50KGV2ZW50KTtcclxufSk7XHJcblxyXG4vLyBJbml0IGV2ZW50IGNoYW5nZSBieSB1c2VyQ29udGFjdCBmaWVsZFxyXG5jaGFuZ2VVc2VyQ29udGFjdC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCAoZXZlbnQpID0+IHtcclxuICAgIGlucHV0VXNlckNvbnRhY3RFdmVudChldmVudCk7XHJcbn0pO1xyXG5cclxubGV0IGZvcm1TdWJtaXQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc3VibWl0Jyk7XHJcbi8vIEluaXQgZXZlbnQgY2hhbmdlIGJ5IHVzZXJDb250YWN0IGZpZWxkXHJcbmZvcm1TdWJtaXQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoZXZlbnQpID0+IHtcclxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBsZXQgZGF0YSA9IHtcclxuICAgICAgICAnbmFtZSc6ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbmFtZScpKS52YWx1ZSxcclxuICAgICAgICAnZmluZCc6ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZmluZCcpKS52YWx1ZSxcclxuICAgICAgICAncmVjZWl2ZSc6ICg8SFRNTElucHV0RWxlbWVudD5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncmVjZWl2ZScpKS52YWx1ZSxcclxuICAgICAgICAndXNlckNvbnRhY3QnOiAoPEhUTUxJbnB1dEVsZW1lbnQ+ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3VzZXJDb250YWN0JykpLnZhbHVlXHJcbiAgICB9O1xyXG4gICAgc3VibWl0KGV2ZW50LCBkYXRhKTtcclxufSk7Il19
