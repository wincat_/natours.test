var

    // Customs path
    mainHost                = "natours.test",
    pathUrlTheme            = "",
    pathUrlApp              = pathUrlTheme + "app",
    pathUrlDev              = pathUrlTheme + "app/dev",
    pathUrlDist             = pathUrlTheme + "app/dist",
    pathNodeModules         = "node_modules",
    pathHeaderFileDefault   = pathUrlTheme + "template/common/",
    pathHeaderFileDist      = pathUrlTheme + "template/common/",
    //--

    gulp                    = require('gulp'),
    sass                    = require('gulp-sass'),
    browserCync             = require ('browser-sync'),
    concat                  = require ('gulp-concat'),
    uglifyJS                = require ('gulp-uglifyjs'),
    cssnano                 = require ('gulp-cssnano'),
    rename                  = require ('gulp-rename'),
    del                     = require ('del'),
    imagemin                = require ('gulp-imagemin'),
    pngqant                 = require ('imagemin-pngquant'),
    cache                   = require ('gulp-cache'),
    autoprefixer            = require ('gulp-autoprefixer'),
    inject                  = require('gulp-inject-string'),

    //TypeScript
    ts                      = require("gulp-typescript"),
    tsProject               = ts.createProject("tsconfig.json"),

    browserify              = require("browserify"),
    source                  = require('vinyl-source-stream'),
    tsify                   = require("tsify"),

    uglify                  = require('gulp-uglify'),
    // sourcemaps              = require('gulp-sourcemaps'),
    // buffer                  = require('vinyl-buffer'),

    watchify                = require("watchify"),
    gutil                   = require("gulp-util");
    // paths                   = { pages: [ pathUrlDev + '/*.php'] };



// Збираємо всі sass крім all-libs.sass файли і переробляємо у css
gulp.task('sass', function () {
  return gulp.src( [ '!'+pathUrlDev + '/sass/all-libs.sass', pathUrlDev + '/sass/**/*.sass', pathUrlDev + '/sass/**/*.scss'] )
      .pipe(sass())
      .pipe(autoprefixer(['last 16 versions', '> 1%', 'ie 8', 'ie 7'], {cascade:true}))
      .pipe(gulp.dest( pathUrlDev + '/css-compile' ))
      .pipe(browserCync.reload({stream: true}));
});

// Збираємо sass файл all-libs.sass і переробляємо у css з добавленням префіксів
gulp.task('sass-libs-compile', function () {
    return gulp.src( pathUrlDev + '/sass/all-libs.sass' )
        .pipe(sass())
        .pipe(autoprefixer(['last 16 versions', '> 1%', 'ie 8', 'ie 7'], {cascade:true}))
        .pipe(gulp.dest( pathUrlDev + '/css-compile' ));
});

// Добавлення до Dev css autoprefixer
gulp.task('css',function () {
    return gulp.src( pathUrlDev + '/css/*.css' )
        .pipe(autoprefixer(['last 16 versions', '> 1%', 'ie 8', 'ie 7'], {cascade:true}))
        .pipe(gulp.dest( pathUrlDev + '/css-compile' ))
        .pipe(browserCync.reload({stream: true}));
});

// Створення .min.css з Dev css, і добавлення autoprefixer,
gulp.task('css-min',function () {
    return gulp.src( pathUrlDev + '/css/*.css' )
        .pipe(autoprefixer(['last 16 versions', '> 1%', 'ie 8', 'ie 7'], {cascade:true}))
        .pipe(cssnano({discardUnused: false}))
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest( pathUrlDev + '/css-compile' ))
        .pipe(browserCync.reload({stream: true}));
});

// Створення JS libs
gulp.task('scripts', function () {
    return gulp.src([
        // libs add

    ])
        .pipe(uglifyJS())
        .pipe(concat('all-libs.js'))
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest( pathUrlDev + '/js' ));
});

// (Створення css libs) з sass файла, берем створений через sass libs.css і перейменовуємо у libs.min.css
gulp.task('css-all-libs', ['sass-libs-compile'], function () {
    return gulp.src( pathUrlDev + '/css-compile/all-libs.css' )
        .pipe(autoprefixer(['last 16 versions', '> 1%', 'ie 8', 'ie 7'], {cascade:true}))
        .pipe(cssnano({discardUnused: false}))
        .pipe(rename({suffix:'.min'}))
        .pipe (gulp.dest( pathUrlDev + '/css-compile' ));
});

// Створення .min.css з sass файлів, які уже стали .css з папки css-compile, та .css без приставки .min.css крім all-libs.css
gulp.task('sassAndCssMin', ['sass'], function () {
    return gulp.src( [pathUrlDev + '/css-compile/*.css', '!'+pathUrlDev + '/css-compile/*min.css', '!'+pathUrlDev + '/css-compile/all-libs.css'] )
        .pipe(autoprefixer(['last 16 versions', '> 1%', 'ie 8', 'ie 7'], {cascade:true}))
        .pipe(cssnano({discardUnused: false}))
        .pipe(rename({suffix:'.min'}))
        .pipe (gulp.dest( pathUrlDev + '/css-compile' ));
});

gulp.task('browser-sync',function () {
    browserCync({
        proxy: mainHost,
        notify:false
    });
});

gulp.task('clean',function () {
   return del.sync( pathUrlDist );
});

gulp.task('clear',function () {
   return cache.clearAll();
});

gulp.task('img',function () {
    return gulp.src( pathUrlDev + '/img/**/*' )
        .pipe(cache( imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins:[{removeViewBox: false}],
            une:[pngqant()]
        })))
        .pipe(gulp.dest( pathUrlDist + '/img' ));
});

// Заміна шляхів у файлах для build
gulp.task('inject:replace', function(){
    return gulp.src( pathUrlTheme + 'index.php' )
        .pipe(inject.replace(pathUrlApp +'/dev/css-compile/all-libs.min.css', pathUrlApp +'/dist/css/all-libs.min.css'))
        .pipe(inject.replace('/css-compile/', '/css/'))
        .pipe(inject.replace('/dev/', '/dist/'))
        .pipe(inject.replace('main.css', 'main.min.css'))
        .pipe(inject.replace('fonts.css', 'fonts.min.css'))
        .pipe(rename('index.php'))
        .pipe(gulp.dest( pathUrlDist ));
});

//TypeScript
var watchedBrowserify = watchify(browserify({
    basedir: '.',
    debug: true,
    entries: [ pathUrlDev+'/js/TypeScript/main.ts'],
    cache: {},
    packageCache: {}
})
    .plugin(tsify));

// gulp.task("copyPhp", function () {
//     return gulp.src(paths.pages)
//         .pipe(gulp.dest( pathUrlTheme ));
// });

function bundle() {
    return watchedBrowserify
        .bundle()
        .pipe(source('bundleMain.js'))
        .pipe(gulp.dest(pathUrlDev+'/js'));
}

gulp.task("typeScript", bundle);
watchedBrowserify.on("update", bundle);
watchedBrowserify.on("log", gutil.log);

gulp.task('watchTS',['typeScript'], function () {
    gulp.watch( pathUrlDev + '/js/TypeScript/*.ts',['typeScript']);
});

gulp.task('watch',[
    'browser-sync',
    // 'scripts',
    'typeScript',
    'sass',
    'sass-libs-compile',
    'css',
    'css-min',
    'css-all-libs',
    'sassAndCssMin'
], function () {
    gulp.watch( pathUrlDev + '/js/TypeScript/*.ts',['typeScript']);
    gulp.watch( pathUrlDev + '/sass/**/*.sass',['sass']);
    gulp.watch( pathUrlDev + '/sass/**/*.scss',['sass']);
    gulp.watch( [ pathUrlDev + '/css-compile/*.css', '!'+pathUrlDev + '/css-compile/*min.css'],['sassAndCssMin']);
    gulp.watch( pathUrlDev + '/css/**/*.css',['css']);
    gulp.watch( pathUrlDev + '/css/**/*.css',['css-min']);
    gulp.watch( pathUrlTheme + '**/*.php').on('change',browserCync.reload);
    // gulp.watch( '*.php').on('change',browserCync.reload);
    gulp.watch( pathUrlApp + '/**/*.css').on('change',browserCync.reload);
    // gulp.watch( pathUrlApp + '/js/**/*.js').on('change',browserCync.reload);
});

gulp.task('build', [
    'clean',
    'img',
    'sass',
    // 'sass-libs-compile',
    'css',
    'css-min',
    'css-all-libs',
    'sassAndCssMin',
    'scripts',
    'inject:replace'
    // 'compressJs'
], function () {

    var buildCss = gulp.src([
        pathUrlDev + '/css-compile/*min.css'
    ])
        .pipe(gulp.dest( pathUrlDist + '/css' ));

    var buildFonts = gulp.src( pathUrlDev + '/fonts/**/*')
        .pipe(gulp.dest( pathUrlDist + '/fonts' ));

    // var buildJs = gulp.src( pathUrlDev + '/js/**/*')
    var buildJs = gulp.src( pathUrlDev + '/js/*.js')
        .pipe(gulp.dest( pathUrlDist + '/js' ));
});